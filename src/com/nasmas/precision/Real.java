package com.nasmas.precision;

public class Real {

    private static int significant32s = 2;
    private static boolean outputInDecimal = true;
    private static int numMantissaBits = 32*significant32s;
    private int[] mantissa;
    private long exponent;
    private boolean sign;

    private static Real Zero = new Real(true, 0, 0);
    private static Real One = new Real(1);
    private static Real E = null;
    private static Real PI = null;
    private static Real OneOverLog2E = null;
    private static int digitsToPrint = 0;
    
    public static void setDigitsToPrint(int digits) {
        digitsToPrint = digits;
    }
    
    public static void setSignificant32s(int s) {
        significant32s = s;
        numMantissaBits = 32*significant32s;
        One = new Real(1);
        Zero = new Real(true, 0, 0);
    }

    public Real(boolean sign, long exponent, int... mantissa) {
        this.sign = sign;
        copyMant(mantissa);
        this.exponent = exponent;
        normalize();
    }

    public Real(int i) {
        newMantissa();
        this.sign = true;
        if (i<0) {
            this.sign = false;
            i = -i;
        }
        this.exponent = 32;
        this.mantissa[0] = i;
        normalize();
    }
    
    public Real() {
        this.sign = true;
        newMantissa();
        this.exponent = 0;
    }
    
    private static Real LongToReal(long n) {
        boolean sign = (n >= 0);
        if (! sign) n = -n;
        int upper = (int) (n >>> 32);
        int lower = (int) n;
        Real x = new Real(sign, 64, upper, lower);
        x.normalize();
        return x;
    }
    
    public static Real euler() {
        if (E == null) calculateEulerConstant();
        return E.dup();
    }
    
    public static Real oneOverLog2E() {
        if (OneOverLog2E == null) {
            OneOverLog2E = One.div(euler().log2());
        }
        return OneOverLog2E;
    }
    
    public static Real PI() {
        if (PI == null) calculatePi();
        return PI.dup();
    }
    
    private static void calculatePi() {
        PI = new Real(0);
        Pi piGen = new Pi();
        piGen.init(numMantissaBits, 2);
        fillFromBitGenerator(PI, 0, piGen);
        PI.exponent = 2;
    }
    
    private static void calculateEulerConstant() {
        E = new Real(2);    // start with 2.0 and fill in the rest
        EulersConstant egen = new EulersConstant();
        egen.init(numMantissaBits, 2);
        fillFromBitGenerator(E, 2, egen);
    }

    private static void fillFromBitGenerator(Real x, int offset, DigitGenerator gen) {
        for (int i=0; i<significant32s; i++) {
            int place = 31;
            if (i == 0) place -= offset;  // skip 'offset' bits
            for (; place >= 0; place--) {
                int bit = gen.nextDigit();
                if (bit == 1)
                    x.mantissa[i] |= (bit << place);
            }
        }
        if (gen.nextDigit() == 1) x.roundUp();
    }
    
    private void zeroMantissa() {
        for (int i=0; i<significant32s; i++)
            mantissa[i] = 0;
    }
    
    private void newMantissa() {
        this.mantissa = new int[significant32s];
        this.zeroMantissa();
    }
    
    private void copyMant(int[] mant) {
        newMantissa();
        int limit = Integer.min(mant.length, significant32s);
        for (int i=0; i<limit; i++) {
            this.mantissa[i] = mant[i];
        }
    }
    
    private int[] dupMantissa() {
        int[] mant = new int[significant32s];
        for (int i=0; i<significant32s; i++) {
            mant[i] = mantissa[i];
        }
        return mant;
    }
    
    private Real dup() {
        return new Real(sign, exponent, mantissa);
    }
    
    private boolean isZero() {
        for (int part : mantissa) {
            if (part != 0) return false;
        }
        return true;
    }

    private void shiftLeft(long n) {
        if (n > numMantissaBits) {
            zeroMantissa();
            return;
        }
        while (n >= 32) {
                for (int i=0; i<significant32s-1; i++) {
                mantissa[i] = mantissa[i+1];
            }
            mantissa[significant32s-1] = 0;
            n -= 32;
        }
        if (n==0) return;
        int carry = 0;
        int carryMask = (-1) << n;
        int right = 32 - (int)n;
        for (int i=significant32s-1; i>=0; i--) {
            int oldCarry = carry;
            carry = (carryMask & mantissa[i]) >>> right;
            mantissa[i] <<= n;
            mantissa[i] |= oldCarry;
        }
    }
    
    private boolean shiftLeft() {
        return shiftLeft(mantissa);
    }
    
    private boolean shiftLeft(int[] mantissa) {
        boolean carry = false;
        boolean next;
        for (int i=mantissa.length-1; i>=0; i--) {
            next = mantissa[i] < 0;
            mantissa[i] <<= 1;
            if (carry) mantissa[i]++;
            carry = next;
        }
        return carry;
    }

    private void shiftRight(long n) {
        if (n > numMantissaBits) {
            zeroMantissa();
            return;
        }
        while (n >= 32) {
            for (int i=significant32s-1; i>=1; i--) {
                mantissa[i] = mantissa[i-1];
            }
            n -= 32;
            mantissa[0] = 0;
        }
        if (n == 0) return;
        int carry = 0;
        int left = 32 - (int)n;
        int carryMask = (-1) >>> left;
        for (int i=0; i<significant32s; i++) {
            int oldCarry = carry;
            carry = (mantissa[i] & carryMask) << left;
            mantissa[i] >>>= n;
            mantissa[i] |= oldCarry;
        }
    }
    
    private void shiftRight() {
        int carry = 0;
        int next;
        for (int i=0; i<significant32s; i++) {
            next = mantissa[i] & 1;
            mantissa[i] >>>= 1;
            if (carry == 1) {
                mantissa[i] |= 0x80000000;
            }
            carry = next;
        }
    }
    
    private Real normalize() {
        if (mantissa[0] < 0) return this;  // already normalized
        if (isZero()) {
            exponent = 0;
            return this;
        }
        int bitsToShift = 0;
        for (int i=0; i<significant32s; i++) {
            if (mantissa[i] == 0) {
                bitsToShift += 32;
                continue;
            }
            bitsToShift += Integer.numberOfLeadingZeros(mantissa[i]);
            break;
        }
        shiftLeft(bitsToShift);
        exponent -= bitsToShift;
        return this;
    }

    private int[] addPartialProducts(int[][] addends) {
        int[] result = new int[significant32s+1];
        // add them up
        long carry = 0;
        for (int k=significant32s; k>=0; k--) {
            long sum = carry;
            for (int i=0; i<addends.length; i++) {
                sum += Integer.toUnsignedLong(addends[i][k]);
            }
            carry = sum >>> 32;
            result[k] = (int)sum;
        }
        return result; 
    }
    
    public Real add(Real other) {
        Real z;
        if (this.sign == other.sign) {
            z = this.addMagnitudes(other);
            z.sign = this.sign;
        }
        else if (this.magnitudeGE(other)) {
            z = subtractLargerFromSmaller(this, other);
            z.sign = this.sign;
        }
        else {
            z = subtractLargerFromSmaller(other, this);
            z.sign = other.sign;
        }
        return z;
    }
    
    public Real addMagnitudes(Real other) {
        if (this.isZero()) return other.dup();
        if (other.isZero()) return this.dup();
        Real x = this.dup();
        Real y = other.dup();
        long xexp = x.exponent;
        long yexp = y.exponent;
        if (xexp < yexp) {
            Real z = x;
            long zexp = xexp; 
            x = y;
            xexp = yexp;
            y = z;
            yexp = zexp;
        }
        // now y's exponent is less than or equal to x's
        // need to shift y's bits to the right to align with x's bits
        long bitsToShift = xexp - yexp;
        y.shiftRight(bitsToShift);
        int carry = 0;
        Real z = new Real(true, 0, 0, 0);
        for (int i=significant32s-1; i>=0; i--) {
            long sum = Integer.toUnsignedLong(x.mantissa[i]) + Integer.toUnsignedLong(y.mantissa[i]) + carry;
            carry = ((sum & 0x100000000L) == 0) ? 0 : 1;
            z.mantissa[i] = (int)sum;
        }
        z.exponent = x.exponent;
        if (carry == 1) {
            z.shiftRight();
            z.mantissa[0] |= 0x80000000;
            z.exponent++;
        }
        return z;
    }

    public Real sub(Real other) {
        Real z;
        if (this.sign != other.sign) {
            z = this.addMagnitudes(other);
            z.sign = this.sign;
        }
        else if (this.magnitudeGE(other)) {
            z = subtractLargerFromSmaller(this, other);
            z.sign = this.sign;
        }
        else {
            z = subtractLargerFromSmaller(other, this);
            z.sign = ! other.sign;
        }
        return z;
    }
    
    // x has greater magnitude than y
    private static Real subtractLargerFromSmaller(Real larger, Real smaller) {
        Real x = larger.normalize();
        Real y = smaller.dup().normalize();
        long bitsToShift = x.exponent - y.exponent;
        y.shiftRight(bitsToShift);
        int borrow = 0;
        Real z = new Real(true,0);
        for (int i=significant32s-1; i>=0; i--) {
            long difference = Integer.toUnsignedLong(x.mantissa[i]) - borrow - Integer.toUnsignedLong(y.mantissa[i]);
            borrow = (difference < 0) ? 1 : 0;
            z.mantissa[i] = (int)difference;
        }
        z.exponent = x.exponent;
        // borrow should be zero since |x| <= |y|
        return z;
    }
    
    // adds one to the lowest significant bit
    private void roundUp() {
        boolean carry = false;
        for (int i=significant32s-1; i>=0; i--) {
            mantissa[i]++;
            carry = mantissa[i] == 0;
            if (carry) continue;
            break;
        }
        if (carry) {
            // each int in mantissa was all 1's, result is power of two
            exponent++;
            mantissa[0] = 0x80000000;
        }
    }
    
    public Real mul(Real other) {
        if (this.isZero() || other.isZero()) {
            return new Real(0);
        }
        Real x = this.dup();
        Real y = other.dup();
        long product;
        int[][] addends;
        long[] xmant = new long[significant32s];
        for (int i=0; i<significant32s; i++)
            xmant[i] = Integer.toUnsignedLong(x.mantissa[i]);
        long[] ymant = new long[significant32s];
        for (int i=0; i<significant32s; i++)
            ymant[i]  = Integer.toUnsignedLong(y.mantissa[i]);
        // only need addends for products that won't get shifted off the end
        int numAddends = significant32s * (significant32s + 3) / 2 - 1;
        // keep additional byte of mantissa for each addend
        addends = new int[numAddends][significant32s+1];
        int addendIndex = 0;
        for (int i=0; i<significant32s; i++) {
            for (int j=0; j<significant32s; j++) {
                // can ignore partial products that would get shifted off the end
                // they get shifted i+j bytes, so i+j < significant32s+1
                if (i+j >= significant32s+1) break;
                product = xmant[i] * ymant[j];
                addends[addendIndex][i+j] = (int)(product >>> 32);
                if (i+j+1 < significant32s+1)
                    addends[addendIndex][i+j+1] = (int)product;
                addendIndex++;
            }
        }
        int[] productMantissa = addPartialProducts(addends);
        long productExponent = x.exponent + y.exponent;
        while (productMantissa[0] >= 0) {
            shiftLeft(productMantissa);
            productExponent--;
        }
        Real z = new Real(this.sign == other.sign, productExponent, productMantissa);
        if (productMantissa[significant32s] < 0)
            z.roundUp();
        return z;
    }
    
    /*
    private void takeaway(Real other) {
        int borrow = 0;
        for (int i=significant32s-1; i>=0; i--) {
            long difference = UnsignedIntToLong(this.mantissa[i]) - borrow - UnsignedIntToLong(other.mantissa[i]);
            borrow = (difference < 0) ? 1 : 0;
            this.mantissa[i] = (int) difference;
        }
    }
    */
    
    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof Real)) return false;
        Real other = (Real)obj;
        if (this.isZero()) {
            if (other.isZero()) return true;
            else return false;
        }
        if (other.isZero()) return false;
        if (this.sign != other.sign) return false;
        this.normalize();
        other.normalize();
        if (this.exponent != other.exponent) return false;
        return this.equalMantissa(other);
    }
    
    private boolean equalMantissa(Real other) {
        for (int i=0; i<significant32s; i++) {
            if (this.mantissa[i] != other.mantissa[i]) return false;
        }
        return true;
    }
    
    public Real div(Real y) {
        this.normalize();
        if (y.isZero()) throw new RuntimeException("division by zero");
        if (this.isZero()) return new Real(true, 0);
        Real a = this.dup();
        Real denominator = y.dup();
        // if numerator's mantissa is less than than denominator's mantissa
        // then first bit of quotient will be zero and second bit will be one.
        // in that case we can run the loop an extra iteration, and adjust
        // the exponent at the end
        boolean extraBitNeeded = (! a.mantissaGE(denominator));
        int iterations = numMantissaBits;
        if (extraBitNeeded) {
            denominator.shiftRight();
        }
        Real q = new Real(true, 0);
        boolean carryOutOfa = false;
        for (int k=0; k<iterations; k++) {
            int bit = (a.mantissaGE(denominator) || carryOutOfa) ? 1 : 0;
            q.shiftLeft();
            if (bit == 1) {
                q.mantissa[significant32s-1]++;
                // do inline subtract to avoid new Reals and for efficiency
                int borrow = 0;
                for (int i=significant32s-1; i>=0; i--) {
                    long difference = Integer.toUnsignedLong(a.mantissa[i]) - borrow - Integer.toUnsignedLong(denominator.mantissa[i]);
                    borrow = (difference < 0) ? 1 : 0;
                    a.mantissa[i] = (int) difference;
                }
            }
            carryOutOfa = a.shiftLeft();
        }
        q.exponent = this.exponent - denominator.exponent + (extraBitNeeded ? 0 : 1);
        q.sign = (this.sign == denominator.sign);
        return q;
    }
    
    public Real pow(long power) {
        Real z = new Real(1);
        Real factor = this;
        int signum = Long.signum(power);
        if (signum == -1) power = -power;
        while (power != 0) {
            if ((power & 0x1) == 1) {
                z = z.mul(factor);
            }
            power >>= 1;
            if (power == 0) break;
            factor = factor.mul(factor);
        }
        if (signum == -1) {
            z = One.div(z);
        }
        return z;
    }
    
    private static boolean unsignedIntsGE(int n1, int n2) {
        // if differ in sign, then the negative one is larger (hi bit is 1)
        if (n1 < 0 && n2 >= 0) return true; 
        if (n2 < 0 && n1 >= 0) return false;
        // same sign
        return (n1 >= n2);
    }
    
    private boolean mantissaEQ(Real other) {
        for (int i=0; i<significant32s; i++) {
            if (this.mantissa[i] != other.mantissa[i]) {
                return false;
            }
        }
        return true;
    }
    
    private boolean mantissaGE(Real other) {
        for (int i=0; i<significant32s; i++) {
            if (this.mantissa[i] != other.mantissa[i]) {
                return unsignedIntsGE(this.mantissa[i], other.mantissa[i]); 
            }
        }
        return true;
    }
    
    public boolean GT(Real other) {
        if (this.GE(other)) {
            return ! this.equals(other);
        }
        return false;
    }
    
    public boolean LE(Real other) {
        return other.GE(this);
    }
    
    public boolean LT(Real other) {
        return other.GT(this);
        
    }
    
    public boolean GE(Real other) {
        if (this.sign != other.sign) return this.sign;
        long thisExp = this.normalize().exponent;
        long otherExp = other.normalize().exponent;
        if (thisExp > otherExp) return this.sign;
        else if (thisExp == otherExp) {
            this.normalize();
            other.normalize();
            if (this.sign)
                return mantissaGE(other);
            else
                return other.mantissaGE(other);
        }
        else return false;
    }
    
    private boolean magnitudeGE(Real other) {
        // every Real has magnitude >= zero
        if (other.isZero()) return true;
        // zero is not GE any nonzero Real
        if (this.isZero()) return false;
        long myExponent = this.normalize().exponent;
        long otherExponent = other.normalize().exponent; 
        if (myExponent != otherExponent)
            return (myExponent > otherExponent);
        // must actually test mantissa
        this.normalize();
        other.normalize();
        return this.mantissaGE(other);
    }
    
    /**
     * Returns the integer part of Real, if it will fit in a long.
     * Caller must guarantee that exponent is no larger than 63 
     * 
     * @return
     */
    public long toLong() {
        if (this.isZero()) return 0;
        long k = this.exponent;
        if (k > 63) throw new RuntimeException("cannot convert real of this size to long");
        if (k <= 0) return 0;
        if (k <= 32) {
            // all bits are in first element of mantissa
            return (long) (mantissa[0] >>> (32-k));
        }
        else {
            k -= 32;
            // need to shift k bits from high end of second element into low end of first
            long result = Integer.toUnsignedLong(mantissa[0]) << k;
            result |= (long) (mantissa[1] >>> (32-k));
            return result;
        }
    }
    
    public Real frac() {
        if (this.isZero()) return Zero.dup();
        long k = this.exponent;
        if (k <= 0) return this.dup();
        if (k > numMantissaBits)
            return Zero.dup();
        Real x = this.dup();
        int i;
        for (i = 0; k >= 32; i++, k-=32) {
            x.mantissa[i] = 0;
        }
        if (k == 0) return x;
        int mask =  ~0x80000000 >>> k;   // mask with 0's in first k bits, 1's elsewhere
        x.mantissa[i] &= mask;
        return x;
    }
    
    public Real integer() {
        Real a = this.dup();
        if (this.exponent > numMantissaBits)  // no fractional bits
            return a;
        else {
            long bitsToShift = numMantissaBits - this.exponent;
            a.shiftRight(bitsToShift);
            a.shiftLeft(bitsToShift);
        }
        a.normalize();
        return a;
    }
    
    public Real floor() {
        Real a = integer();
        if ( ! this.sign)
            a = a.sub(One);
        return a;
    }
    
    private Real fraction() {
        this.normalize();
        if (this.exponent <= 0) return this;
        this.shiftLeft(this.exponent);
        this.exponent = 0;
        return this;
    }
    
    private int decimalDigit() {
        if (1 <= this.exponent && this.exponent <= 4) {
            int floor = this.mantissa[0] >>> (32 - this.exponent);
            return floor; 
        }
        else if (this.exponent < 1)  // x < 1
            return 0;
        throw new RuntimeException("shouldn't happen");
    }
    
    static Real powerOf2(long power) {
        Real x = One.dup();
        x.exponent = power + 1;
        return x;
    }
    
    static boolean GE(Real x, Real y) {
        if (x.sign != y.sign) return x.sign;
        
        return true;
    }
    
    public Real log2() {
        if (this.isZero()) throw new RuntimeException("logarithm of zero undefined");
        Real x = this.dup();
        x.normalize();
        // start with the integer part of log
        Real log = LongToReal(x.exponent-1);
        if (x.mantissaEQ(One)) {
            return log;
        }
        int k = 0;
        // divide x by 2**(log) so 1<=x<2
        x.exponent = 1;
        Real two = powerOf2(1);
        while (true) {
            while (two.GE(x)) {
                x = x.mul(x);
                k--;
            }
            if (k < log.exponent - numMantissaBits)
                break;
            log = log.add(powerOf2(k));
            // divide x by 2
            x.exponent--;
        }
        return log;
    }
    
    public boolean isInfinite() {
        return this.exponent == Long.MAX_VALUE;
    }
    
    private Real infinity(boolean sign) {
        return new Real(sign, Long.MAX_VALUE, 0x80000000);
    }
    
    public Real ln() {
        return this.log2().mul(Real.oneOverLog2E());
    }
    
    public Real exp() {
        if (this.isZero()) return One.dup();
        this.normalize();
        if (this.exponent > 62) {
            if (this.sign)
                return infinity(true);
            else
                return Zero.dup();
        }
        long integerPart = this.toLong();
        Real powerOfE;
        if (integerPart > 0) {
            powerOfE = euler().pow(integerPart);
        }
        else
            powerOfE = One.dup();
        Real x = this.dup().fraction();
        x.sign = true;
        if (x.isZero()) return powerOfE;
        /*
        * Taylor series expansion
         * e^x = 1 + x + x^2/2! + x^3/3! + ...
         * */
        Real result = One.dup();
        Real oneOverNFactorial = One.dup();
        Real xToTheN = One.dup();
        int n = 1;
        while (true) {
            Real realN = new Real(n);
            oneOverNFactorial = oneOverNFactorial.div(realN);
            xToTheN = xToTheN.mul(x);
            Real term = xToTheN.mul(oneOverNFactorial);
            result = result.add(term);
            term.normalize();
            if (term.exponent < -numMantissaBits) break;
            n++;
        }
        result = result.mul(powerOfE); 
        if (! this.sign) {
            result = One.div(result);
        }
        return result;
    }
    
    public boolean isNegative() {
        return ! this.sign;
    }
    
    private static Real RecipA = null;
    private static Real RecipB = null;
    private static int RecipIterations = 0;
    
    public Real recip() {
        if (RecipA == null) {
            RecipA = new Real(48).div(new Real(17));
            RecipB = new Real(32).div(new Real(17));
        }
        Real d = this.dup();
        d.exponent = 0;   // algorithm wants 0.5 < x < 1.0
        // x1 = A - BD
        Real x = RecipA.sub(RecipB.mul(d));
        if (RecipIterations == 0) {
            double ln2 = Math.log(2);
            double log2Of17 = Math.log(17) / ln2;
            double places = numMantissaBits + 1;
            double y = places / log2Of17;
            y = Math.log(y) / ln2;
            RecipIterations = (int)Math.ceil(y);
        }
        for (int i=0; i<RecipIterations; i++) {
            // x = x + x(1 - Dx)
            x = x.add(x.mul(One.sub(d.mul(x))));
        }
        x.exponent -= this.exponent;
        return x;
    }
    
    private static int SqrtIterations = 0;
    
    public Real sqrtNewton() {
        if (this.isZero()) return Zero.dup();
        if (this.isNegative()) throw new RuntimeException("square root of negative number");
        long k = this.exponent;
        long n = k/2;
        Real x = One.dup();
        x.exponent = n;
        this.exponent = (k & 1);  // 1 if k is odd, 0 if k is even
        // a = number to take sqrt of is now between 1/2 and 2.
        if (SqrtIterations == 0) {
            double log2d = Math.log(numMantissaBits)/Math.log(2);
            SqrtIterations = (int) Math.ceil(log2d);
        }
        for (int i=0; i<SqrtIterations; i++) {
            // x = 1/2 (x + a/x)
            x = this.div(x).add(x).normalize();
            x.exponent--;
        }
        this.exponent = k;
        x.exponent += n;
        return x;
    }
    
    public String decimalString() {
        StringBuffer out = new StringBuffer();
        if (this.isNegative()) out.append('-');
        Real x = this.dup();
        x.normalize();
        if (x.isZero()) return "0.0";
        Real ten = new Real(10);
        //System.out.println("in decimalString, x = " + x.binaryString());
        //System.out.println("in decimalString, ten = " + ten.binaryString());
        Real log2x = x.log2();
        // System.out.println("in decimalString, log2(x) = " + log2x.binaryString());
        Real log2ten = ten.log2();
        //System.out.println("in decimalString, log2(ten) = " + log2ten.binaryString());
        Real log10x = log2x.div(log2ten);
        //System.out.println("in decimalString, log10(x) = " + log10x.binaryString());
        long powerOfTenL = log10x.toLong();
        if (log10x.isNegative()) powerOfTenL = -powerOfTenL - 1;
        Real tenToTheNth = ten.pow(-powerOfTenL);
        Real z = this.mul(tenToTheNth);
        int digit = z.decimalDigit();
        // for numbers that are exact powers of 10, log10x sometimes is (n-1) followed by all 1 bits
        // this causes the first digit to overflow
        if (digit == 10) {
            out.append("1.0");
            powerOfTenL++;
        }
        else {
            out.append(digit);
            out.append('.');
        }
        z = z.fraction();
        int bitsConsumed = 0;
        int outputDigits = 0;
        while (bitsConsumed < numMantissaBits) {
            z = z.mul(ten);
            digit = z.decimalDigit();
            out.append(digit);
            z = z.fraction();
            bitsConsumed += 4;
            outputDigits++;
            if (digitsToPrint > 0 && outputDigits > digitsToPrint)
                break;
        }
        out.append("E" + powerOfTenL);
        return out.toString();
    }
    
    @Override
    public String toString() {
        if (outputInDecimal)
            return this.decimalString();
        else
            return this.binaryString();
    }
        
    private String binaryString() {
        this.normalize();
        StringBuffer result = new StringBuffer();
        if (!sign) result.append('-');
        int[] mant = this.dupMantissa();
        result.append('.');
        int numbits = significant32s * 32;
        for (int i=0; i<numbits; i++) {
            if (mant[0] < 0)
                result.append('1');
            else
                result.append('0');
            shiftLeft(mant);
            if (i%4 == 3) result.append(' ');
        }
        result.append('B');
        result.append(exponent);
        return result.toString();
    }    
    
    public static void main(String[] args) {
        Real.setSignificant32s(4);
        Real r = new Real(true, 32, 1, 1);
        System.out.println("r:" + r);
        // System.out.println("  = " + r.decimalString());
        Real x = r.sub(One);
        System.out.println("r minus 1:" + x);
        Real f = x.dup();
        System.out.println("first term:" + f);
        x = x.mul(x);
        x = x.div(new Real(2));
        System.out.println("squared over 2:" + x);
        f = f.sub(x);
        // System.out.println("log to two terms:" + f);
        System.out.println("log r:" + r.log2());
        Real two = new Real(true, 32, 2, 0);
        System.out.println("two:" + two);
        System.out.println("log:" + two.log2());
        Real z = r.add(two);
        System.out.println("sum:" + z);
        System.out.println("log:" + z.log2());
        z = z.add(z);
        System.out.println("twice sum:" + z);
        System.out.println("log:" + z.log2());
        Real thirty = new Real(30);
        System.out.println("thirty:" + thirty);
        z = z.add(thirty)
                .add(r);
        System.out.println("should be 37 and some:" + z);
        z = z.mul(thirty);
        System.out.println("times 30:" + z);
        z = z.sub(thirty);
        System.out.println("minus 30:" + z);
        Real zero = z.sub(z);
        System.out.println("minus itself:" + zero);
        z = thirty.sub(z);
        System.out.println("30 minus z:" + z);
        Real seven = new Real(7);
        System.out.println("7 :" + seven);
        Real ten = new Real(10);
        z = ten.div(seven);
        System.out.println("10 over 7:" + z);
        System.out.println("10 :" + ten);
        System.out.println("7 :" + seven);
        System.out.println("7 over 10:" + seven.div(ten));
        z = ten.pow(0);
        System.out.println("10**0 :" + z);
        z = ten.pow(1);
        System.out.println("10**1 :" + z);
        z = ten.pow(2);
        System.out.println("10**2 :" + z);
        z = ten.pow(3);
        System.out.println("10**3 :" + z);
        z = ten.pow(10);
        System.out.println("10**10 :" + z);
        z = ten.pow(100);
        System.out.println("10**100 :" + z);
        z = ten.pow(-1);
        System.out.println("10**-1 :" + z);
        z = ten.pow(-2);
        System.out.println("10**-2 :" + z);
        Real e = Real.euler();
        System.out.println("e:" + e);
        z = new Real(1);
        System.out.println("e^e :" + e.exp());
        System.out.println("e^1 :" + z.exp());
        Real ln2 = One.div(Real.euler().log2());
        System.out.println("ln(2) :" + ln2);
        Real pi = Real.PI();
        System.out.println("pi :" + pi);
        Real recip = pi.recip();
        System.out.println("1/pi :" + recip);
        System.out.println("1/pi :" + One.div(pi));
        Real sqrtPi = pi.sqrtNewton();
        System.out.println("sqrt(pi) : " + sqrtPi);
        z = sqrtPi.mul(sqrtPi);
        System.out.println("sqrt(pi) squared : " + z);
        System.out.println("difference from pi : " + z.sub(pi));
        z = new Real(-3).div(new Real(2));
        System.out.println("-1.5 : " + z);
        z = z.exp();
        System.out.println("exp(-1.5) : " + z);
        z = z.ln();
        System.out.println("ln(exp(-1.5)) : " + z);
        z = new Real(200);
        int iters = 100000;
        long begin = System.currentTimeMillis();
        for (int i=0; i<iters; i++) {
            recip = pi.add(z);
        }
        long end = System.currentTimeMillis();
        System.out.println("add took " + (end-begin));
        begin = System.currentTimeMillis();
        for (int i=0; i<iters; i++) {
            recip = pi.sub(z);
        }
        end = System.currentTimeMillis();
        System.out.println("sub took " + (end-begin));
        begin = System.currentTimeMillis();
        for (int i=0; i<iters; i++) {
            recip = pi.mul(pi);
        }
        end = System.currentTimeMillis();
        System.out.println("mul took " + (end-begin));
        begin = System.currentTimeMillis();
        for (int i=0; i<iters; i++) {
            recip = pi.mul(pi);
        }
        end = System.currentTimeMillis();
        System.out.println("mul took " + (end-begin));
        begin = System.currentTimeMillis();
        for (int i=0; i<iters; i++) {
            recip = One.div(pi);
        }
        end = System.currentTimeMillis();
        System.out.println("div took " + (end-begin));
        begin = System.currentTimeMillis();
        for (int i=0; i<iters; i++) {
            recip = pi.recip();
        }
        end = System.currentTimeMillis();
        System.out.println("recip took " + (end-begin));
        begin = System.currentTimeMillis();
        for (int i=0; i<iters; i++) {
            recip = pi.sqrtNewton();
        }
        end = System.currentTimeMillis();
        System.out.println("sqrt took " + (end-begin));
        System.out.println("end");
    }
}
