package com.nasmas.precision;

public class EulersConstant implements DigitGenerator {
    private int[] bRep;
    private int len;
    private int base;
    
    public void init(int n, int base) {
        len = 8;
        double target = (n+5) * Math.log(base);
        if (len < target && target > 8)
            len = (int)Math.ceil(target);
        // System.out.println("using " + len + " pseudo digits");
        this.base = base;
        bRep = new int[len];
        for (int i=0; i<len; i++) {
            bRep[i] = 1;
        }
    }
    
    public int nextDigit() {
        int quotient = 0;
        for (int i=len-1; i>=2; i--) {
            int value = base*bRep[i] + quotient;
            quotient = (value)/i;
            bRep[i] = (value % i);
        }
        return quotient;
    }
    
    public static void main(String[] args) {
        EulersConstant e = new EulersConstant();
        int numDigits = 100;
        e.init(numDigits, 10);
        System.out.print("2.");
        for (int i=0; i<numDigits; i++) {
            System.out.print(e.nextDigit());
            if ((i % 4) == 3) System.out.print(' ');
        }
        System.out.println();
        e.init(64, 2);
        System.out.print("10.");
        for (int i=0; i<64; i++) {
            System.out.print(e.nextDigit());
            if ((i % 4) == 3) System.out.print(' ');
        }
        System.out.println();
        e.init(64, 16);
        System.out.print("2.");
        for (int i=0; i<64; i++) {
            System.out.print(Integer.toHexString(e.nextDigit()));
            if ((i % 4) == 3) System.out.print(' ');
        }
    }
}
