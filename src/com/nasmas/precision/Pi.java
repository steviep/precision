package com.nasmas.precision;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class Pi implements DigitGenerator {
    private int[] cRep;
    private int len;
    private ArrayList<Integer> queue;
    private int base;
    
    public void init(int n, int base) {
        this.base = base;
        double logError = 2 + (n + 5) * Math.log(base) / Math.log(2.0);
        len = (int) Math.ceil(logError);
        cRep = new int[len];
        for (int i=0; i<len; i++) {
            cRep[i] = 2;
        }
        queue = new ArrayList<Integer>();
        if (base == 2) queue.add(0);
    }
    
    public int digit() {
        int quotient = 0;
        for (int i=len-1; i>=2; i--) {
            int value = base*cRep[i] + quotient;
            int modulo = 2*i - 1;
            quotient = value/modulo;
            cRep[i] = (value % modulo);
            quotient *= (i-1);
        }
        int digit = base*cRep[1] + quotient;
        quotient = digit / base;
        cRep[1] = digit % base;
        return quotient;
    }
    
    // Sometimes the "integer" part of the mixed base number is deficient because
    // the "fractional" part is in [0, 2) instead of [0, 1)
    // This causes the next "digit" to be equal to the base (ie 10 in the base)
    // In these cases we need to propagate a carry to the left.  If the immediate
    // left digit is (base - 1) then the carry will propagate further.
    // Thus we need to hold the last "safe" (ie. less than (base - 1)) digit in a queue
    // along with any following instances of (base - 1), so that we can do a carry
    // into the "safe" digit without affecting earlier digits.
    public List<Integer> getSomeDigits() {
        int baseM1 = base - 1;
        List<Integer> result = new ArrayList<Integer>();
        while (true) {
            int digit = this.digit();
            if (digit == base) {
                for (int i=0; i<queue.size(); i++) {
                    Integer predigit = queue.get(i);
                    if (predigit == baseM1)
                        result.add(0);
                    else
                        result.add(predigit+1);
                }
                queue.clear();
                queue.add(0);
            }
            else if (digit == baseM1) {
                queue.add(digit);
            }
            else {
                result.addAll(queue);
                queue.clear();
                queue.add(digit);
                if (result.size() > 0) break;
            }
        }
        return result;
    }
    
    private List<Integer> releasedDigits = new ArrayList<>();
    
    public int nextDigit() {
        if (releasedDigits.size() == 0) {
            List<Integer> more = getSomeDigits();
            releasedDigits.addAll(more);
        }
        return releasedDigits.remove(0);
    }
    
    public static void main(String[] args) {
        Pi pi = new Pi();
        int numDigits = 102;
        pi.init(numDigits, 10);
        int i = 0;
        Integer digit;
        FileWriter f = null;
        try {
            f = new FileWriter("pi.txt");
            while (i<numDigits) {
                digit = pi.nextDigit();
                f.write("" + digit);
                i++;
                if ((i%80) == 0) f.write("\n");
                if (i >= numDigits) break;
            }
            f.write("\n");
            f.close();
        } catch(Exception e) { throw new RuntimeException(e); } 
        numDigits = 64;
        pi.init(numDigits, 2);
        i = 0;
        while (i<numDigits) {
            digit = pi.nextDigit();
            System.out.print(digit);
            System.out.print(' ');
            i++;
            if (i >= numDigits) break;
        }
        System.out.println();
        numDigits = 20;
        pi.init(numDigits, 16);
        i = 0;
        while (i<numDigits) {
            digit = pi.nextDigit();
            System.out.print(Integer.toHexString(digit));
            System.out.print(' ');
            i++;
            if (i >= numDigits) break;
        }
        System.out.println();
    }
}
