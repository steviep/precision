package com.nasmas.precision;

public class Ln2 {
    private static double tolerance = 0.25/65536.0;
    
    public static int TwoToPowerModK(int power, int k) {
        int result = 1;
        for (int i=0; i<power; i++) {
            result = ((result * 2) % k);
        }
        return result;
    }
    
    public int bit(int n) {
        double sum = 0;
        int k;
        for (k=1; k<=n; k++) {
            int term = TwoToPowerModK(n-k, k);
            sum += 1.0 * term / k;
            if (sum > 1.0) sum -= 1.0;
        }
        double powerOfTwo = 2.0;
        double term;
        k = n+1;
        do {
            term = 1.0 / (k * powerOfTwo);
            sum += term;
            if (sum > 1.0) sum -= 1.0;
            powerOfTwo *= 2.0;
            k++;
        } while (term > tolerance);
        return (int) (2 * sum);
    }

    public static void main(String[] args) {
        Ln2 obj = new Ln2();
        int fourBits = 0;
        for (int i=0; i<64; i++) {
            int bit = obj.bit(i);
            fourBits <<= 1;
            fourBits += bit;
            if (i%4 == 0) {
                System.out.print(Integer.toHexString(fourBits));
                fourBits = 0;
                if (i%16 == 15) System.out.print(' ');

            }
        }
        System.out.println();
        double log2 = Math.log(2.0);
        System.out.println(Double.toHexString(log2));
    }
}
