package com.nasmas.precision;

public interface DigitGenerator {
    public void init(int numDigis, int base);
    public int nextDigit();
}
